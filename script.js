console.log("hello World");

let name = "Lola";
console.log(name);

var age = 35;
var otherAge = "35"

if(age==otherAge){
    console.log("double equal same1");
}

if(age===otherAge){
    console.log("triple equal same2");
}console.log("not same");

function sayHello(name){
    while(true){
    //let keyword is only visible within the while loop (always use let)    
    let variable1 = "Fred";
    //var keyword is only visible within the whole function
    var variable2 = "Barnie";

    // below will work because it is set as a global keyword, 
    // but not goog practice. Should use let or var
    variable3 = "Wilma";
    // makes it a final variable
    const variable4 = "John"
    }
    console.log(sayHello("Lola"))
    console.log(variable1)
    console.log(variable2)
    return "hello " + name;
}

console.log(sayHello("Lola"))
//console.log(variable1)
//console.log(variable2)
console.log(variable3)