function sayHello(name="Fred"){
    return "Hello " + name;
}
console.log(sayHello("Barnie"));

//without the parentheis after sayHello, it is just going to return a function pointer rather than the actual value
let result = sayHello;
let someValue = result("Wilma");
console.log(result);
console.log(someValue);

let myFunction = function(){
    return "hello";
}