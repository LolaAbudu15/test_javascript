// below is lambda function
// name is the parameter and will return hello plus whatever name is passed into the function
let someFunction = (name) => ("hello " + name);

console.log(someFunction("Fred"));

// for more than 1 statements in the body, use the {} and put the return statement
let someFunction = (name) => {
     ("hello " + name);
    return ("hi " + name);
}