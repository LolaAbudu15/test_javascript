// define a simple function that returns
function myFunction(age){
    return "Hello, you are " + age + " years old";
}

// create another variable pointing to it
let newFunction = myFunction;

// call your function using a new pointer
let result = newFunction(31);
console.log(result);

// redefine your function as a lambda
let lambdaFunction = (age) => ("lambda - hello you are " + age);
console.log(lambdaFunction(31));
